import Detail from "./DetailMovie";
import Home from "./Home";
import Splash from "./SplashScreen";

export {Detail, Home, Splash};