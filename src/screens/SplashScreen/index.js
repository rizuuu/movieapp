import React, { Component, useEffect } from 'react';
import { View, Text, Image, StyleSheet} from 'react-native';
import Home from '../Home';



const Splash = ({navigation}) =>{
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Home')
    }, 3000)
  }, [navigation])

  return(
    <View style={styles.viewIndex}>
      <View>
          <Image style={styles.Image} source={require('../../images/logo3.png')}/>
      </View>
      <View>
          <Text style={styles.text}>
              MOVIE APP
          </Text>
      </View>      
      <View style={styles.nameUser}>
          <Text style={styles.name}>
              {/* s */}
          </Text>
      </View>
    </View>
  )
}

export default Splash;

const styles = StyleSheet.create({
  viewIndex: {
      backgroundColor: '#FFFFFF',
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      marginTop: -20
  },
  Image: { 
      width: 200,
      height: 160,                 
  },
  text:{
      fontFamily: 'Helvetica',
      fontSize: 25,
      alignItems: 'center',
      fontWeight: 'bold'
  },    
  topSize:{
      paddingTop: 20,
  },
  nameUser:{
    alignContent: 'flex-end',
  }
})